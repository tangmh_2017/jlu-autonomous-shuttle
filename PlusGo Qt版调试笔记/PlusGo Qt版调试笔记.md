# PlusGo Qt版调试笔记

## .pro文件相关的问题

.pro文件内包含项目所需的c++文件和依赖的库文件，拿到手的.pro文件需要根据我们自己的编译环境进行修改

### ROS版本问题

原来的.pro版本使用的ROS版本是melodic，需要根据自己的ros版本修改成自己对应的版本：

`/opt/ros/melodic/`->`/opt/ros/noetic`

### QT += serialport 提示serialport不存在的问题

在项目文件.pro中使用串口类时需要添加serialport，解决这个问题需要装两个库：

```powershell
sudo apt-get install libqt5serialport5
sudo apt-get install libqt5serialport5-dev
```

## CAN相关问题

## 找不到libcontrolcan.so

报错：

```
./APMDriver
./APMDriver: error while loading shared libraries: libcontrolcan.so: cannot open shared object file: No such file or directorypowershell
```

这个问题是关于周立功CAN工具的，

首先安装周立功驱动，在官网下载就可以，下载地址：

https://manual.zlg.cn/web/#/146

选择CAN卡驱动的第一项：

![](.\img\zlgcan.PNG)

然后解压，进行安装,USBCAN-II新版驱动基于libusb实现，请确保运行环境中有libusb-1.0的库。
如果是ubuntu，可连网在线安装，命令如下：

`apt-get install libusb-1.0-0`

将`libusbcan.so`拷到`/lib`目录。

进入`test`目录，不带参数运行测试程序，会打印CAN测试参数说明：

`./test`

加入参数，调用test，可进行收发测试：

`./test 4 0 3 0x1400 2 0 3 1000`

CAN驱动库的调用demo在`test.c`中，可参考进行二次开发。

设备调试常用命令：

1、查看系统是否正常枚举到usb设备，打印它们的VID/PID（USBCAN为0471:1200）：

`lsusb`

2、查看系统内所有USB设备节点及其访问权限：

`ls /dev/bus/usb/ -lR`

3、修改usb设备的访问权限使普通用户可以操作，其中xxx对应lsusb输出信息中的bus序号，yyy对应device序号：
	`# chmod 666 /dev/bus/usb/xxx/yyy`

4、如果要永久赋予普通用户操作USBCAN设备的权限，需要修改udev配置，增加文件：`/etc/udev/rules.d/50-usbcan.rules`，内容如下：
	`SUBSYSTEMS=="usb", ATTRS{idVendor}=="0471", ATTRS{idProduct}=="1200", GROUP="users", MODE="0666"`

重新加载udev规则后插拔设备即可应用新权限：

`udevadm control --reload`

安装完成之后需要把 `libcontrolcan.so`文件复制到'/lib'文件夹中，这个文件所在的目录是：`autodrive/plusgo-application/CAN/zlgcan/amd64`

复制命令如下：

`~/Documents/PlusGo/autodrive/plusgo-application/CAN/zlgcan/amd64$ sudo cp libcontrolcan.so /lib`


目前遇到的问题就是这几项，解决之后软件可以编译通过，然后运行编译好的软件就可以打开操作界面：

![](./img/Screenshot from 2023-04-11 15-27-27.png)
